﻿angular.module('core.settingapi', ['ngResource','core']);
angular.
  module('core.settingapi').
  factory('SettingApi', ['$resource','Core',
    function ($resource,Core
        ) {
        var self = this;
        self.table = 'settings';
        self.idfield = 'settingid';
      
        var Methods = {
		        'getAll': {
		            'method':'GET',
		            'url': Core.apiRootAddress + self.table+'/_all_docs',
		            'params': {
		                'include_docs':true
		            },
		            'isArray':true,
		            'transformResponse':function(data) {
		                var returnOb = angular.fromJson(data);
		                var results =[];
		                for (var i=0; i < returnOb.rows.length; i++) {
						  results.push(returnOb.rows[i].doc);
						}; 
		                return results;
		            }
		        },
		        'get': {
		            'method':'GET',
		            'url': Core.apiRootAddress + self.table+ '/:_id',
		            'params': {
		                'include_docs':true,
		            },
		            'isArray':false,
		        }, 
		        'update': { method: 'PUT' }
		    };
        self.url =  Core.apiRootAddress + self.table+'/:'+self.idfield;

        return $resource(self.url, {}, Methods);
    }
  ]);