﻿angular.module('core',
    [
        'pascalprecht.translate'
    ]);
angular.
  module('core').
  factory('Core', 
  ['$translate',
   '$http',
   '$location', '$window',
    function (
    $translate,
    $http,
    $location,
    $window
        ) {
        var self = this;

		
		self.roundToTwo = function(n){n = n *100; n = Math.round(n); return parseFloat(n)/100;};
        self.currentUser = 1;
        self.taxrates = {tax1:{value:.07},tax2:{value:.01}};

        //The API
        return {
            roundToTwo: self.roundToTwo,
            taxrates: self.taxrates,
            currentCurrency:self.currentCurrency
          };
    }
  ]);