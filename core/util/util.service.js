angular.module('core.utilapi', ['ngResource','core']);
angular.
  module('core.utilapi').
  factory('UtilApi', ['$resource','Core',
    function ($resource,Core
        ) {
        
        var self = this;
        self.table = 'tickets';//table;
        self.idfield = 'ticketid';//idfield;
         var Methods = {
		        'getAll': {
		            'method':'GET',
		            'url': Core.apiRootAddress + self.table+'/_all_docs',
		            'params': {
		                'include_docs':true
		            },
		            'isArray':true,
		            'transformResponse':function(data) {
		                var returnOb = angular.fromJson(data);
		                var results =[];
		                for (var i=0; i < returnOb.rows.length; i++) {
						  results.push(returnOb.rows[i].doc);
						}; 
		                return results;
		            }
		        },
				'getByUser': {
		            'method':'GET',
		            'url': Core.apiRootAddress + self.table+'/_design/by-user/_view/by-user',
		            'data':true,
		            'transformResponse':function(data) {
		                var returnOb = angular.fromJson(data);
		                var results =[];
		                for (var i=0; i < returnOb.rows.length; i++) {
						  results.push(returnOb.rows[i].value);
						}; 
		                return results;
		            }
		        },
		        'getTotals': {
		            'method':'GET',
		            'url': Core.apiRootAddress + self.table+'/_design/by-user/_view/total_closed',
		            'isArray':true,
		            'transformResponse':function(data) {
		                var returnOb = angular.fromJson(data);
		                var results =[];
		                for (var i=0; i < returnOb.rows.length; i++) {
						  results.push(returnOb.rows[i].value);
						}; 
		                return results;
		            }
		        },
		        'update': { method: 'PUT' }
		    };
        self.url =  Core.apiRootAddress + self.table+'/:'+self.idfield;

        return $resource(self.url, {}, Methods);
        
    }
  ]);