﻿angular.module('core.tabledb', []);
angular.
  module('core.tabledb').
  factory('TableDB', [
    function (
        ) {
        var self = this;
        self.table = 'tblTable';
        self.idfield = 'tableid';

        /*
        self.get = function (id, fn) { LocalDB.setTableAndIdField(self.table, self.idfield); LocalDB.get(id, fn); };
        self.getAll = function (fn,filter) { LocalDB.setTableAndIdField(self.table, self.idfield); LocalDB.getAll(fn,filter); };
        self.save = function (data, index, modified, fn) { LocalDB.setTableAndIdField(self.table, self.idfield); LocalDB.save(data, index, modified, fn); };
        self.remove = function (id, fn) { LocalDB.setTableAndIdField(self.table, self.idfield); LocalDB.remove(id, fn); };
        self.delete = function (id, fn) { LocalDB.setTableAndIdField(self.table, self.idfield); LocalDB.delete(id, fn); };
        self.removeAll = function (filters, overwrite, fn) { LocalDB.setTableAndIdField(self.table, self.idfield); LocalDB.removeAll(filters, overwrite, fn); };
        self.getCount = function (fn, filter) { LocalDB.setTableAndIdField(self.table, self.idfield); LocalDB.getCount(fn, filter); };
        */
        self.get = function (id, fn) {
            try{
                fn(JSON.parse(localStorage.getItem(self.table + '_' + id)));
            } catch (e) {
                fn(null,e);
            }
        };
        self.getAll = function (fn, filter) {
            var results = [];
            for (var i = 0; i < localStorage.length; i++) {
                if(localStorage.key(i).startsWith(self.table))
                    results.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
            }
            fn(results.filter(filter));
        };
        self.save = function (data, fn) { 
            localStorage.setItem(self.table + '_' + data._id, JSON.stringify(data));
            fn();
        };
        self.remove = function (id, fn) {
            localStorage.removeItem(self.table + '_' + id);
            fn();
        };
        

        return {
            save: self.save,
            get: self.get,
            getAll: self.getAll,
            remove: self.remove,
            
        };
    }
  ]);