﻿/*!
 * Application config
 * Has global configuration, global helper functions, AngularJS init and Cordova init
 */
//TODO: mobile does not fix view, 
//TODO: start pouchdb server with -o option if it fails 


'use strict';

//global config vars: these are referenced inside their respective modules
var googleAPIKey = 'nananananananana';

var devApiRoot = 'http://127.0.0.1:8089/';
//devApiRoot = 'http://192.168.2.157:8089/';
var prodApiRoot = 'http://192.168.1.13:8089/';
//devApiRoot  =  'http://192.168.0.15:8089/';
var apiRootAddress = ((location.hostname.indexOf('localhost') > -1) ? devApiRoot : prodApiRoot);
apiRootAddress = devApiRoot;

function findIndex(list, filter) {
	list.filter
}

var forEach = function (array, index, fn, then) {
	fn();
	if (array.length - 1 > index) {
		index++;
		forEach(array, index, fn, then);
	} else
		then(array);
	return;
};

//Helper function for multiple translations
var multiTranslate = function (tran, defs, index, fn) {
	forEach(defs, index, tran, fn);
};

//Helper function if Null
var ifNull = function (a, b) {
	if (a == null || a == undefined)
		a = b;
	return a;
};
var isNullorEmpty = function (a) {
	if (a == null)
		return true;
	if (('' + a).trim() === '')
		return true;
	return false;
};

//debug and global variables
var _dbg = {};
var debug = true;

//shorter console debug console and optional output to <div>
function cLog(message, object) {
	if (!debug)
		return;
	console.log(message, object);
	$('#debugDiv').html('<p>' + message + ': ' + object + '</p>');
}

//AngularJS init
angular.
module('app').
config(['$locationProvider', '$routeProvider', '$translateProvider', //'UserAuth',
		function config($locationProvider, $routeProvider, $translateProvider //UserAuth
		) {
			$locationProvider.hashPrefix('!');

			$routeProvider
			.when('/calc', {
				template : '<calc></calc>',
				requireLogin : true
			})
			

			.otherwise('/calc');

			$translateProvider.useStaticFilesLoader({
				prefix : 'languages/',
				suffix : '.json'
			});

			$translateProvider.preferredLanguage('en'); //actual language set on userOptions module
			$translateProvider.use('en'); //actual language set on userOptions module
			$translateProvider.useSanitizeValueStrategy('escape');

		}
	]).run(['$rootScope', '$location','SettingApi','Core', 
	function ($rootScope, $location,SettingApi,Core) {
			$rootScope.$on('$routeChangeStart', function (event) {
			
			
			
		})}
	]);