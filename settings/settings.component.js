﻿'use strict';
angular.
  module('settings',['core','core.settingapi']);
angular.
  module('settings')
  .component('settings', {
      templateUrl: 'settings/settings.template.html',
      bindings: {
         
      }
      , controller: ('SettingsController', 
      ['$scope','Core', 'SettingApi','$http',
      function ($scope,Core,SettingApi,$http) {
          var self = this;

          $scope.clearData = function () {
              if (confirm('Clear all data?')) {
                  localStorage.clear();
              }
          };

 		$scope.createViews = function () {
        	$http({
        		url: Core.apiRootAddress + 'tickets/_design/views/_view/by_user',
        		data: {
        			map: 'function(doc) {if(doc.userid === key) {emit(doc._id, doc); }}',
        			reduce: ''
        		},
        		method: 'PUT'
          	});
          	$http({
        		url: Core.apiRootAddress +'tickets/_design/views/_view/total_bystatus',
        		data: {
        			map: 'function(doc) {emit(doc.status, {doc.taxes,doc.total}); }',
        			reduce: '_sum'
        		},
        		method: 'PUT'
          	});
          
        };


          $scope.save = function(){
          	var globals = {
          		_id: 'globals',
          		taxrates: {
          			tax1: {name: 'State',value:$scope.tax1},
          			tax2: {name:'Municipal',value: $scope.tax2}
          		}
          	};
          	SettingApi.save(globals).$promise.then(function(data){
          		globals._rev = data.rev;
          	});
          };
          
          $scope.goBack = function () {
              Core.navigate('/tables');
          };
          
          self.$onInit = function(){
          	SettingApi.get({_id: 'globals',include_documents:true})
          	.$promise.then(function(data){
          		
          		$scope.tax1 = data.taxrates.tax1.value;
          		$scope.tax2 = data.taxrates.tax2.value;
          	});
          	
          };
      }])
  }
  );

