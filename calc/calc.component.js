﻿'use strict';
angular.module('calc', ['core','core.tabledb'
]);
angular.
  module('calc').
  component('calc', {
      templateUrl: 'calc/calc.template.html',
      controller: ('CalcCtrl',
          ['$scope', '$routeParams','$filter', 'Core','TableDB',
              function ($scope, $routeParams, $filter,Core,TableDB) {
                  var self = this;
				 
				 //TODO: create copy for simpler calculator app?
				 //TODO: move all fields to readonly
				 //TODO: check where the taxes come from
				 //TODO: CLOSE TICKET
				 //TOOD: REOPEN TICKET?
				 
 				  $scope.TAXRATE = 11.5;
				  $scope.TAX = '$0.00';
				  $scope.SUBTOTAL = 0;
				  $scope.TIP = 0;
				  $scope.TIPPERCENT = 0;
				  
				  $scope.close = function(){
				  	Core.currentTicket.status = 'CLOSED';
				  	$scope.save(Core.currentTicket,function(){
				  		Core.navigate('/tables');
				  	});
				  };

				$scope.save = function(table,fn){
					TableDB.save(table,function(d){
						fn();
					});
				};
				  $scope.calcTip = function(percent) {
				      var tippercent = parseFloat(percent);
				      var subtotal = parseFloat($scope.SUBTOTAL);
				      $scope.TIPPERCENT = Core.roundToTwo(percent);
				      var tip = subtotal * tippercent;
				      $scope.TIP = 
				      	//Core.roundToTwo(subtotal * tippercent);
				      	$filter('currency')(tip, '$', 2);
					  $scope.updateTotal();
				  };
				  $scope.clearSubtotal = function(){
				    $scope.SUBTOTAL = '';
				  };
				  $scope.calcTax = function() {
					$scope.updateTotal();  
				  };
				  
				  $scope.updateTotal = function(){
					  var taxrate = parseFloat($scope.TAXRATE);
					  var subtotal = parseFloat($scope.SUBTOTAL);
					  var tax= taxrate / parseFloat(100) * subtotal;
					  var tippercent = parseFloat($scope.TIPPERCENT);
					   var tip = subtotal * tippercent;
					   $scope.TIP = $filter('currency')(tip, '$', 2);
					  
					  var total = subtotal+tip+tax;
					  
					  //$scope.TAX = Core.roundToTwo(parseFloat($scope.TAXRATE) / parseFloat(100) * parseFloat($scope.SUBTOTAL));
					  //$scope.TIP=Core.roundToTwo($scope.SUBTOTAL*$scope.TIPPERCENT);
					  $scope.TAX = $filter('currency')(tax, '$', 2);

					 // alert(total);
					  $scope.TOTAL = total;//Core.roundToTwo(subtotal+tip+tax);
					     //$filter('currency')(total, '$', 2);
					  //$scope.TOTAL = ($scope.SUBTOTAL+$scope.TIP+$scope.TAX);
				  };

				  $scope.saveTaxRate = function(){
				     localStorage.setItem('TTC_TaxRate','{"TAXRATE": "'+$scope.TAXRATE+'"}');
				  };

				  $scope.loadTaxRate = function(){
				      var taxrate =  null;//localStorage.getItem('TTC_TaxRate');
				      taxrate = taxrate || '{"TAXRATE": "11.5"}';
				      $scope.TAXRATE = parseFloat(JSON.parse(taxrate).TAXRATE);
				  };
				  
				  $scope.goBack = function(){
					  Core.navigate('/ticket')
				  };

                  self.$onInit = function () {
                    Core.currentTicket = Core.currentTicket||{lines:[],total:0}
					$scope.SUBTOTAL = Core.currentTicket.total;
					$scope.loadTaxRate();
                    $scope.updateTotal();
					
                  };
              }]
    )
  });