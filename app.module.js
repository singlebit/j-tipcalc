﻿
'use strict';

// Define the `App` module
//var App =
    angular.module('app', [
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngAnimate',
    'ngAria',
    'pascalprecht.translate',
    'calc',
	'core.settingapi',
	
]);